import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Login from './Screens/Login'
import Home from './Screens/Home'
// import Register from './Screens/Register'

function App() {
  return (
      <Router>
        <Switch>
          <Route path="/" exact  component={Login} />
          <Route path="/home" component={Home} />
          {/* <Route path="/register" component={Register} /> */}
          {/* <Route path="/dpm" component={Dpm} /> */}
          {/* <Route path="/waiting" component={Waiting} /> */}
          {/* <Route path="/setting" component={Setting} /> */}
          {/* <Route path="/pdf" component={PdfView} /> */}
        </Switch>
      </Router>
  );
}

export default App;