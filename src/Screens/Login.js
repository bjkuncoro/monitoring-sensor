import React,{useState,useContext,useEffect} from 'react'
import axios from 'axios'
// import {GlobalStateContext} from './globalDataContext'
import { useHistory } from "react-router-dom";
import { useMediaQuery } from 'react-responsive'

function Login() {
    const [state, setState] = useState({
        nim: null,
        password: null,
        tpsname:'',
        a:true,
        count:0,
      });
    
    const isTabletOrMobile = useMediaQuery({ maxWidth: 1200 })
    const [flexIsMobile,setFlex]    =   useState(2)
    const [flexDisMobile,setFlexD]    =   useState('column')
    const [heightIsMobile,setHight]    =   useState('none')
    // const [stateReducer, dispatch] = useContext(GlobalStateContext);
    let history =   useHistory()
    const   [showPetunjuk,setShoePetunjuk]  =   useState(false)
    useEffect(() => {
       console.log('isedesktop',isTabletOrMobile)
       if(isTabletOrMobile==false){
           setFlex(5);
           setFlexD('row')
           setHight('100vh')
       }
    //    if(stateReducer.dataUser==undefined){

    //    }else{
    //        history.push('/dashboard')
    //    }
    }, [])

    const sendlogin =   ()=>{
        axios.post('http://203.24.51.52:2000/login',{
            username:state.nim,password:state.password
        }).then(res=>{
            console.log(res.data)
            if(res.data.login===true){
                history.push('/home',{fromLogin:true})
            }else{
                alert('Terjadi Kesalahan')
            }
        }).catch(err=>{
            console.log({err})
        })
    }

    return (
        <div style={{display:'flex',width:'100%',height:heightIsMobile,backgroundColor:'whitesmoke',justifyContent:'center',alignItems:'center',flexDirection:flexDisMobile}}>
      
            <div style={{display:'flex',flex:flexIsMobile,width:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                {/* <div style={{display:'inline-block',overflow:'hidden',height:'80%',width:'100%',}}> */}
                    <img src={require('../Images/ini.png')} style={{width:'100%'}}/>
                {/* </div> */}
            </div>
            <div style={{display:'flex',flex:5,width:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                <div style={{display:'flex',width:'80%',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                    <h3 style={{color:'darkslategray'}}>
                        Login Monitoring Sensor
                    </h3>
                    {/* <h4 style={{color:'darkslategray',textAlign:'center',fontWeight:'300',paddingLeft:30,paddingRight:30,margin:0}}>
                        Untuk Petunjuk Silahkan Liat Disini
                    </h4> */}
                    {/* <a href='http://dashboard.presensi.untan.ac.id/cek-nih' style={{color:'darkslategaray',marginTop:10,marginBottom:10,width:80,backgroundColor:'#fff',textAlign:'center',textDecoration:'none',justifyContent:'center',padding:10,borderRadius:8,borderStyle:'solid',borderWidth:1}}>
                        Disini
                    </a> */}
                    {/* <button 
                        onClick={()=>{
                            console.log('test');
                            setShoePetunjuk(true)
                        }}
                        style={{color:'darkslategaray',marginTop:10,marginBottom:10,width:80,backgroundColor:'#fff',textAlign:'center',textDecoration:'none',justifyContent:'center',padding:10,borderRadius:8,borderStyle:'solid',borderWidth:1}}>
                            Petunjuk
                    </button>  */}
                </div>
                <div style={{display:'flex',height:330,width:'80%',justifyContent:'center',alignItems:'center',flexDirection:'column',backgroundColor:'#fff',borderRadius:20,marginBottom:50}}>
                    <div style={{display:'flex',width:'100%',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10}}>
                        {/* <h3 style={{color:'dodgerblue'}}>
                            NIM
                        </h3> */}
                        <input 
                        onChange={e=>{setState({ ...state, nim: e.target.value });}}
                        type='text' placeholder={'NIP/NIH (Email Jika Tidak Ada)'} name='nim' style={{paddingLeft:5,fontSize:15,height:40,width:'70%',backgroundColor:'#fff',borderWidth:2,borderColor:'gainsboro',borderStyle:'solid',borderRadius:8}}/>
                    </div>
                    <div style={{display:'flex',width:'100%',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10}}>
                        {/* <h3 style={{color:'dodgerblue'}}>
                            NIM
                        </h3> */}
                        <input 
                        onChange={e=>{setState({ ...state, password: e.target.value });}}
                        type='password' placeholder={'Tgl Lahir (Ex. 24051995)'} name='nim' style={{paddingLeft:5,fontSize:15,height:40,width:'70%',backgroundColor:'#fff',borderWidth:2,borderColor:'gainsboro',borderStyle:'solid',borderRadius:8}}/>
                    </div>
                    <div style={{display:'flex',width:'100%',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10}}>
                    <button 
                            onClick={()=>{
                                console.log('test');
                                sendlogin()
                                // this.props.history.push('/dashboard')
                            }}
                            style={{padding:0,width:'70%',height:40,marginTop:10,backgroundColor:'dodgerblue',color:'white',borderRadius:8,fontSize:16,borderWidth:'inherit'}}>
                                <h3 style={{color:'#fff',margin:0,fontSize:15}}>
                                    Login
                                </h3>
                        </button> 
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
