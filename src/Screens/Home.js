import React, { useState,useEffect,useContext } from 'react'
import Ripples from 'react-ripples'
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import RefreshIcon from '@material-ui/icons/Refresh';
import Apps from '@material-ui/icons/Apps';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import Clock from 'react-live-clock';
import axios    from    'axios';
import { useHistory,useLocation } from "react-router-dom";
import {Line} from 'react-chartjs-2';
import moment from "moment";

function Home() {
    const [dataKerja,setDataKerja]    =   useState([]);
    const [dataTanggal,setDataTanggal]    =   useState([]);
    const [lux, setlux] = useState([])
    const [luxTime, setluxTime] = useState([])
    const [suhu, setsuhu] = useState([])
    const [suhuTime, setsuhuTime] = useState([])
    const [tegangan, settegangan] = useState([])
    const [teganganTime, setteganganTime] = useState([])
    const [lembab, setlembab] = useState([])
    const [lembabTime, setlembabTime] = useState([])
    const [lampu1, setlampu1] = useState(0)
    const [lampu2, setlampu2] = useState(0)
    const [kipas1, setkipas1] = useState(0)
    const [kipas2, setkipas2] = useState(0)
    const location  =   useLocation()
    const history   =   useHistory()  
    const getDataInterval   =   ()=>{
        axios.get('http://203.24.51.52:2000/getDataSensor').then(res=>{
            console.log(res.data)
            if(res.data[0].kipas1!==null){
                setkipas1(Number(res.data[0].kipas1))
            }
            if(res.data[0].kipas2!==null){
                setkipas2(Number(res.data[0].kipas2))
            }
            if(res.data[0].lampu1!==null){
                setlampu1(Number(res.data[0].lampu1))
            }
            if(res.data[0].lampu2!==null){
                setlampu2(Number(res.data[0].lampu2))
            }
            const arus =   res.data.filter(i=>{
                return i.jenis  ==  "arus"
            })
            const lux =   res.data.filter(i=>{
                return i.jenis  ==  "lux"
            })
            const suhu =   res.data.filter(i=>{
                return i.jenis  ==  "suhu"
            })
            const kelembaban =   res.data.filter(i=>{
                return i.jenis  ==  "kelembaban"
            })
            const tegangan =   res.data.filter(i=>{
                return i.jenis  ==  "tegangan"
            })
            console.log(kelembaban)
            const k = arus.map(i=>{
                return i.nilai
            })
            const j = arus.map(i=>{
                const  l = new Date(i.createdAt).toLocaleTimeString().split(' ')[0]
                // console.log(l)
                return l
            })
            const q = lux.map(i=>{
                return i.nilai
            })
            const w = lux.map(i=>{
                const  l = new Date(i.createdAt).toLocaleTimeString().split(' ')[0]
                // console.log(l)
                return l
            })
            const e = suhu.map(i=>{
                return i.nilai
            })
            const r = suhu.map(i=>{
                const  l = new Date(i.createdAt).toLocaleTimeString().split(' ')[0]
                // console.log(l)
                return l
            })
            const t= tegangan.map(i=>{
                return i.nilai
            })
            const y = tegangan.map(i=>{
                const  l = new Date(i.createdAt).toLocaleTimeString().split(' ')[0]
                // console.log(l)
                return l
            })
            const u = kelembaban.map(i=>{
                return i.nilai
            })
            const o = kelembaban.map(i=>{
                const  l = new Date(i.createdAt).toLocaleTimeString().split(' ')[0]
                // console.log(l)
                return l
            })
            // const size = 30;
            // const l = k.slice(0, size).map(i => {
            //     return i
            // })
            // const m = j.slice(0, size).map(i => {
            //     return i
            // })
            setlembab(u)
            setlembabTime(o)
            settegangan(t)
            setteganganTime(y)
            setsuhu(e)
            setsuhuTime(r)
            setlux(q)
            setluxTime(w)
            setDataKerja(k)
            setDataTanggal(j)
        }).catch(err=>{
            console.log({err})
        })
    }

    useEffect(() => {
        const interval = setInterval(() => {
                getDataInterval()
        }, 2000);
        return () => clearInterval(interval);
    }, []);

    useEffect(() => {
        console.log(location.state)
        if(!location.state){
            history.push('/')
        }
        // axios.get('http://203.24.51.52:2000/getDataSensor').then(res=>{
        //     console.log(res.data)
        //     const k = res.data.map(i=>{
        //         return i.nilai
        //     })
        //     const j = res.data.map(i=>{
        //         return i.id
        //     })
        //     const size = 30;
        //     const l = k.slice(0, size).map(i => {
        //         return i
        //     })
        //     const m = j.slice(0, size).map(i => {
        //         return i
        //     })
        //     setDataKerja(l)
        //     setDataTanggal(m)
        // }).catch(err=>{
        //     console.log({err})
        // })
    }, [])

    const dataArus = {
        labels: dataTanggal,
        datasets: [
          { label: 'Grafik Arus', fill: false, lineTension: 0.1, backgroundColor: 'rgba(75,192,192,0.4)', borderColor: 'rgba(75,192,192,1)', borderCapStyle: 'butt', borderDash: [], borderDashOffset: 0.0, borderJoinStyle: 'miter', pointBorderColor: 'rgba(75,192,192,1)', pointBackgroundColor: '#fff', pointBorderWidth: 1, pointHoverRadius: 5, pointHoverBackgroundColor: 'rgba(75,192,192,1)', pointHoverBorderColor: 'rgba(220,220,220,1)', pointHoverBorderWidth: 2, pointRadius: 1, pointHitRadius: 10, data: dataKerja, }
        ]
    };
    const dataLux = {
        labels: luxTime,
        datasets: [
          { label: 'Grafik Cahaya', fill: false, lineTension: 0.1, backgroundColor: 'rgba(75,192,192,0.4)', borderColor: 'rgba(75,192,192,1)', borderCapStyle: 'butt', borderDash: [], borderDashOffset: 0.0, borderJoinStyle: 'miter', pointBorderColor: 'rgba(75,192,192,1)', pointBackgroundColor: '#fff', pointBorderWidth: 1, pointHoverRadius: 5, pointHoverBackgroundColor: 'rgba(75,192,192,1)', pointHoverBorderColor: 'rgba(220,220,220,1)', pointHoverBorderWidth: 2, pointRadius: 1, pointHitRadius: 10, data: lux, }
        ]
    };
    const dataSuhu = {
        labels: suhuTime,
        datasets: [
          { label: 'Grafik Suhu', fill: false, lineTension: 0.1, backgroundColor: 'rgba(75,192,192,0.4)', borderColor: 'rgba(75,192,192,1)', borderCapStyle: 'butt', borderDash: [], borderDashOffset: 0.0, borderJoinStyle: 'miter', pointBorderColor: 'rgba(75,192,192,1)', pointBackgroundColor: '#fff', pointBorderWidth: 1, pointHoverRadius: 5, pointHoverBackgroundColor: 'rgba(75,192,192,1)', pointHoverBorderColor: 'rgba(220,220,220,1)', pointHoverBorderWidth: 2, pointRadius: 1, pointHitRadius: 10, data: suhu, }
        ]
    };
    const dataKelembaban = {
        labels: lembabTime,
        datasets: [
          { label: 'Grafik kelembaban', fill: false, lineTension: 0.1, backgroundColor: 'rgba(75,192,192,0.4)', borderColor: 'rgba(75,192,192,1)', borderCapStyle: 'butt', borderDash: [], borderDashOffset: 0.0, borderJoinStyle: 'miter', pointBorderColor: 'rgba(75,192,192,1)', pointBackgroundColor: '#fff', pointBorderWidth: 1, pointHoverRadius: 5, pointHoverBackgroundColor: 'rgba(75,192,192,1)', pointHoverBorderColor: 'rgba(220,220,220,1)', pointHoverBorderWidth: 2, pointRadius: 1, pointHitRadius: 10, data: lembab, }
        ]
    };
    const dataTegangan = {
        labels: teganganTime,
        datasets: [
          { label: 'Grafik Tegangan', fill: false, lineTension: 0.1, backgroundColor: 'rgba(75,192,192,0.4)', borderColor: 'rgba(75,192,192,1)', borderCapStyle: 'butt', borderDash: [], borderDashOffset: 0.0, borderJoinStyle: 'miter', pointBorderColor: 'rgba(75,192,192,1)', pointBackgroundColor: '#fff', pointBorderWidth: 1, pointHoverRadius: 5, pointHoverBackgroundColor: 'rgba(75,192,192,1)', pointHoverBorderColor: 'rgba(220,220,220,1)', pointHoverBorderWidth: 2, pointRadius: 1, pointHitRadius: 10, data: tegangan, }
        ]
    };

    return (
        <div style={{display:'flex',width:'100%',minHeight:'100vh',backgroundColor:'whitesmoke',justifyContent:'flex-start',alignItems:'center',flexDirection:'column'}}>
        <div style={{display:'flex',width:'90%',flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginBottom:0,marginTop:30}}>
            <Ripples>
                <button onClick={()=>{
                    // setShowMenu(true)
                    }} style={{height:40,width:40,borderRadius:5,backgroundColor:'#fff',outline:'none',opacity:1,borderWidth:'inherit'}}>
                    <Apps style={{fontSize:25,color:'darkslategray'}}/>
                </button>
            </Ripples>
            <h4 style={{fontWeight:300}}>
                {/* Hi, {stateReducer.dataUser.data.nama} */}
                {/* Hi, {stateReducer.dataUser!==undefined?stateReducer.dataUser.data.nama:''} */}
            </h4>
            <h3>
                <Clock ticking={true} />
            </h3>
        </div>
        <div style={{display:'flex',width:'90%',height:120,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#fff',borderRadius:20}}>
            <div style={{display:'flex',flex:1,height:'100%',justifyContent:'center',alignItems:'center'}}>
                <img src={require('../Images/privacy-and-policy.png')} style={{width:120}}/>
            </div>
            <div style={{display:'flex',flex:2,height:'100%',justifyContent:'center',alignItems:'flex-start',flexDirection:'column'}}>
                <div style={{display:'flex',flex:2,height:'100%',width:'100%',justifyContent:'center',alignItems:'center',flexDirection:'row',paddingRight:10}}>
                    <div style={{display:'flex',flex:5,justifyContent:'flex-start',alignItems:'center'}}>
                        <h3 style={{margin:0,fontSize:17}}>
                            Monitoring Data Sensor M Agus Prastyo
                        </h3>
                    </div>
                    <div style={{display:'flex',flex:1,justifyContent:'flex-end',alignItems:'center',paddingRight:10}}>
                        <Ripples>
                            <button onClick={()=>{window.location.reload()}} style={{height:40,width:40,borderRadius:10,backgroundColor:'#fff',outline:'none',opacity:1,borderWidth:'inherit'}}>
                                <RefreshIcon style={{fontSize:20,color:'darkslategray'}}/>
                            </button>
                        </Ripples>
                    </div>
                </div>
                <div style={{display:'flex',flex:1,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'row',paddingRight:10}}>
                    <h4 style={{fontWeight:300}}>
                        {/* Unit : {stateReducer.dataUser!==undefined?stateReducer.dataUser.data.unit:''} */}
                    </h4>
                </div>
            </div>
        </div>
        {/* <div style={{display:'flex',width:'90%',height:60,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#fff',borderRadius:20,marginTop:40}}>
            <div style={{display:'flex',flex:1,height:'100%',justifyContent:'center',alignItems:'center'}}>
                <AlarmOnIcon style={{fontSize:30,color:'indianred',marginRight:8}}/>
                <h3 style={{fontSize:17,fontWeight:400,color:'dodgerblue'}}>
                    Status : {status}
                </h3>
            </div>
        </div> */}
        <div style={{display:'flex',width:'90%',height:100,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'indigo',borderRadius:20,marginTop:30}}>
            <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center'}}>
                <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column',paddingLeft:20}}>
                    <h3 style={{fontSize:17,fontWeight:400,color:'#fff',margin:0}}>
                        Status Kipas 1
                    </h3>
                    <h3 style={{fontSize:35,fontWeight:'bold',color:'gold',margin:0}}>
                        {kipas1==0?'OFF':'ON'}
                        {/* {dataJamDatang} */}
                    </h3>
                </div>
            </div>
            <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center'}}>
                <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column',paddingLeft:20}}>
                    <h3 style={{fontSize:17,fontWeight:400,color:'#fff',margin:0}}>
                        Status Kipas 2
                    </h3>
                    <h3 style={{fontSize:35,fontWeight:'bold',color:'gold',margin:0}}>
                        {kipas2==0?'OFF':'ON'}
                        {/* {dataJamDatang} */}
                    </h3>
                </div>
            </div>
        </div>
        <div style={{display:'flex',width:'90%',height:100,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'indianred',borderRadius:20,marginTop:30}}>
            <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                <h3 style={{fontSize:17,fontWeight:400,color:'#fff',margin:0}}>
                    Status Lampu 1
                </h3>
                <h3 style={{fontSize:35,fontWeight:'bold',color:'gold',margin:0}}>
                    {lampu1==0?'OFF':'ON'}
                </h3>
            </div>
            <div style={{display:'flex',flex:3,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                <h3 style={{fontSize:17,fontWeight:400,color:'#fff',margin:0}}>
                    Status Lampu 2
                </h3>
                <h3 style={{fontSize:35,fontWeight:'bold',color:'gold',margin:0}}>
                    {lampu2==0?'OFF':'ON'}
                </h3>
            </div>
        </div>
        <div style={{display:'flex',width:'90%',height:150,flexDirection:'row',justifyContent:'space-between'}}>
            <div style={{display:'flex',flex:1,height:'100%',flexDirection:'row',justifyContent:'flex-start',alignItems:'center',}}>
                <div style={{display:'flex',width:'97%',height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column',backgroundColor:'white',borderRadius:20,marginTop:30}}>
                    {/* <h3 style={{fontSize:17,fontWeight:600,color:'darkslategrey'}}>
                        Grafik Kerja
                    </h3> */}
                    <Line data={dataArus} options={{ maintainAspectRatio: false,scales: { yAxes: [{ ticks: {max:300, stepSize: 100 } }] } }} />
                </div>
            </div>
            <div style={{display:'flex',flex:1,height:'100%',flexDirection:'row',justifyContent:'flex-end',alignItems:'center',}}>
                <div style={{display:'flex',width:'97%',height:'100%',justifyContent:'center',alignItems:'flex-end',flexDirection:'column',backgroundColor:'white',borderRadius:20,marginTop:30}}>
                    {/* <h3 style={{fontSize:17,fontWeight:600,color:'darkslategrey'}}>
                        Grafik Kerja
                    </h3> */}
                    <Line data={dataLux} options={{ maintainAspectRatio: false,scales: { yAxes: [{ ticks: {max:300, stepSize: 100 } }] } }} />
                </div>
            </div>
        </div>
        <div style={{display:'flex',width:'90%',height:150,flexDirection:'row',justifyContent:'space-between',marginTop:30}}>
            <div style={{display:'flex',flex:1,height:'100%',flexDirection:'row',justifyContent:'flex-start',alignItems:'center',}}>
                <div style={{display:'flex',width:'97%',height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column',backgroundColor:'white',borderRadius:20,marginTop:30}}>
                    {/* <h3 style={{fontSize:17,fontWeight:600,color:'darkslategrey'}}>
                        Grafik Kerja
                    </h3> */}
                    <Line data={dataTegangan} options={{ maintainAspectRatio: false,scales: { yAxes: [{ ticks: {max:300, stepSize: 100 } }] } }} />
                </div>
            </div>
            <div style={{display:'flex',flex:1,height:'100%',flexDirection:'row',justifyContent:'flex-end',alignItems:'center',}}>
                <div style={{display:'flex',width:'97%',height:'100%',justifyContent:'center',alignItems:'flex-end',flexDirection:'column',backgroundColor:'white',borderRadius:20,marginTop:30}}>
                    {/* <h3 style={{fontSize:17,fontWeight:600,color:'darkslategrey'}}>
                        Grafik Kerja
                    </h3> */}
                    <Line data={dataSuhu} options={{ maintainAspectRatio: false,scales: { yAxes: [{ ticks: {max:300, stepSize: 100 } }] } }} />
                </div>
            </div>
        </div>
        <div style={{display:'flex',width:'90%',height:150,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'white',borderRadius:20,marginTop:30,marginBottom:30}}>
            <div style={{display:'flex',flex:1,height:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                {/* <h3 style={{fontSize:17,fontWeight:600,color:'darkslategrey'}}>
                    Grafik Kerja
                </h3> */}
                <Line data={dataKelembaban} options={{ maintainAspectRatio: false,scales: { yAxes: [{ ticks: {max:300, stepSize: 100 } }] } }} />
            </div>
        </div>
    </div>
    )
}

export default Home
